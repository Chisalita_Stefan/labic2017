package main;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import algorithms.Algorithm;
import algorithms.AlgorithmData;
import algorithms.AlgorithmFactory;
import algorithms.AlgorithmType;
import algorithms.CompressionProgressListener;

import javax.swing.JProgressBar;
import javax.swing.ImageIcon;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame implements CompressionProgressListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3147434869452679582L;
	private JPanel contentPane;
	private static MainWindow instance = null;
	private File fileIn = null;
	private File fileOut = null;
	private JLabel lblInputFileName = null;
	private JLabel lblOutputFileName = null;
	private AlgorithmType algorithm;
	private JProgressBar progressBar = null;
	private JLabel lblCompresionName = null;
	private JLabel lblGain = null;
	private JLabel lblTotalTime = null;
	private JLabel lblTimePerChunck = null;

	public static MainWindow getInstance(String title) {
		if (instance == null) {
			instance = new MainWindow();
		}

		instance.setTitle(title);
		return instance;
	}

	/**
	 * Create the frame.
	 */
	private MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnSaveFile = new JButton("Save file...");
		btnSaveFile.setBounds(307, 125, 85, 23);
		JButton btnOpenFile = new JButton("Open file...");
		btnOpenFile.setBounds(307, 91, 87, 23);

		btnOpenFile.addActionListener(e -> chooseInputFile());
		btnSaveFile.addActionListener(e -> chooseOutFile());

		JComboBox<AlgorithmType> comboChooseCompression = new JComboBox<>();
		comboChooseCompression.setBounds(144, 58, 153, 20);

		for (AlgorithmType type : AlgorithmType.values()) {
			comboChooseCompression.addItem(type);
		}

		comboChooseCompression.addActionListener(e -> selectAlgorithm(e));
		comboChooseCompression.setSelectedIndex(0);
		algorithm = AlgorithmType.values()[0];

		JLabel lblChooseCompression = new JLabel("Choose Compression:");
		lblChooseCompression.setBounds(15, 61, 110, 14);

		JButton btnCompress = new JButton("Compress");
		btnCompress.setBounds(307, 57, 99, 23);
		btnCompress.addActionListener(e -> compressData());

		contentPane.setLayout(null);
		contentPane.add(lblChooseCompression);
		contentPane.add(comboChooseCompression);
		contentPane.add(btnCompress);
		contentPane.add(btnOpenFile);
		contentPane.add(btnSaveFile);

		JLabel lblInputFile = new JLabel("Input File:");
		lblInputFile.setBounds(15, 99, 55, 14);
		contentPane.add(lblInputFile);

		lblInputFileName = new JLabel("...........");
		lblInputFileName.setBounds(80, 99, 217, 14);
		contentPane.add(lblInputFileName);

		JLabel lblOutputFile = new JLabel("Output File:");
		lblOutputFile.setBounds(15, 124, 57, 14);
		contentPane.add(lblOutputFile);

		lblOutputFileName = new JLabel("...........");
		lblOutputFileName.setBounds(80, 124, 217, 14);
		contentPane.add(lblOutputFileName);

		progressBar = new JProgressBar(0, 100);
		progressBar.setBounds(15, 443, 534, 14);
		progressBar.setValue(0);

		contentPane.add(progressBar);

		JButton btnStopCompression = new JButton("");
		btnStopCompression.setIcon(new ImageIcon(MainWindow.class.getResource("/com/sun/javafx/scene/control/skin/caspian/dialog-error.png")));
		btnStopCompression.setBounds(559, 437, 25, 20);
		btnStopCompression.addActionListener(e -> stopCompression());
		contentPane.add(btnStopCompression);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(15, 165, (this.getWidth() - 30), 2);
		contentPane.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBounds(15, 178, 569, 99);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblCA = new JLabel("Compression Algorithm:");
		lblCA.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblCA);
		
		lblCompresionName = new JLabel("Unknown");
		panel.add(lblCompresionName);
		
		JLabel lblCG = new JLabel("Compression Gain:");
		lblCG.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblCG);
		
		lblGain = new JLabel("Unknown");
		panel.add(lblGain);
		
		JLabel lblTPC = new JLabel("Time per Chunck:");
		panel.add(lblTPC);
		
		lblTimePerChunck = new JLabel("Unknown");
		panel.add(lblTimePerChunck);
		
		JLabel lblTT = new JLabel("Total Time:");
		panel.add(lblTT);
		
		lblTotalTime = new JLabel("Unknown");
		panel.add(lblTotalTime);
		
		JButton btnShowChart = new JButton("Show Chart");
		btnShowChart.addActionListener(e->showResultsChart());
		btnShowChart.setBounds(10, 288, 89, 23);
		contentPane.add(btnShowChart);


		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	private void showResultsChart() {
		//TODO show the chart		
	}

	private void stopCompression() {
		AlgorithmFactory.killCurrentInstance();
	}

	private void selectAlgorithm(ActionEvent e) {
		@SuppressWarnings("unchecked")
		JComboBox<AlgorithmType> cb = (JComboBox<AlgorithmType>) e.getSource();
		algorithm = (AlgorithmType) cb.getSelectedItem();
		System.out.println(algorithm);
	}

	private void compressData() {
		// TODO ONLY TESTS
		//fileIn = new File("C:/Users/Stefan/Desktop/t.pdf");
		//fileOut = new File("C:/Users/Stefan/Desktop/t.pdf.gzip");

		if (fileIn == null) {
			System.err.println("fileIn is null!");
			return;
		}

		if (fileOut == null) {
			System.err.println("fileOut is null!");
			return;
		}

		Algorithm a = AlgorithmFactory.getNewInstane(algorithm);
		if (a != null) {
			a.registerProgressListener(this);
			a.setProgressBar(progressBar);
			a.compress(fileIn, fileOut);
		}

	}

	private void updateFileLables() {
		if (fileIn != null) {
			lblInputFileName.setText(fileIn.getPath());
		}
		if (fileOut != null) {
			lblOutputFileName.setText(fileOut.getPath());
		}
	}

	private void chooseOutFile() {
		JFileChooser fileChooser = new JFileChooser();
		int returnVal = fileChooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fileOut = fileChooser.getSelectedFile();
			System.out.println("Saving: " + fileOut.getName() + ".");
		}
		updateFileLables();
	}

	private void chooseInputFile() {
		System.out.println("input chooser");
		JFileChooser fileChooser = new JFileChooser();
		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fileIn = fileChooser.getSelectedFile();
			System.out.println("Opening: " + fileIn.getName() + ".");
		}
		updateFileLables();
	}

	@Override
	public void compressionFinished(AlgorithmData data) {
		this.lblCompresionName.setText(data.getAlgorithmName().toString());
		this.lblGain.setText(""+data.getCompresionGain()+"%");
		this.lblTotalTime.setText(""+data.getTotalTime()+"ns");
		this.lblTimePerChunck.setText(""+data.getTimePerChunck()+"ns");
		
		//ResultsChart chart = new ResultsChart(data);
		
		
	}
}
