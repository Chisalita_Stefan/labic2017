package algorithms;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import net.jpountz.lz4.LZ4BlockOutputStream;
import net.jpountz.lz4.LZ4Compressor;


public class LZ4Algorithm extends Algorithm {
	
	private static LZ4Compressor lz4Compressor = null;
	
	
	
	public LZ4Algorithm() {
		super();
		type = AlgorithmType.LZ4;
	}

	/*
	 * returns a LZ4Compressor for performance reasons
	 */
	private static LZ4Compressor getCompressor(){
		if(lz4Compressor == null){
			lz4Compressor =  AlgorithmFactory.getLZ4FactoryInstance().fastCompressor();
		}
		return lz4Compressor;
	}

	@Override
	protected void doCompression(FileInputStream inStream,
			FileOutputStream outStream) {
	
		
		//LZ4Factory factory = AlgorithmFactory.getLZ4FactoryInstance();
		LZ4Compressor compressor = getCompressor();//factory.fastCompressor();//TODO make static?
		LZ4BlockOutputStream blockOutStream = new LZ4BlockOutputStream(outStream, this.CHUNK_SIZE, compressor);
		try {
			copyBetweenStreams(inStream, blockOutStream);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				blockOutStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		/*
		///JUST FOR TESTS
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LZ4BlockInputStream blockIn = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(new File("C:/Users/Stefan/Desktop/t.pdf.gzip"));
			fos = new FileOutputStream(new File("C:/Users/Stefan/Desktop/t_uncompressed.pdf"));
			LZ4FastDecompressor decomp = factory.fastDecompressor();
			blockIn = new LZ4BlockInputStream(fis, decomp);
			copyBetweenStreams(blockIn, fos);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				blockIn.close();
				fis.close();
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		*/
		

	}

}
