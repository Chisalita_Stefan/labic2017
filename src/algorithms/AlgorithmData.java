package algorithms;

public class AlgorithmData {
	private AlgorithmType algorithmName;
	private float compresionGain = 0.0f;
	private long totalTime = 0;
	private long timePerChunck = 0;
	private int chunckTimes = 0;
	
	public AlgorithmData(AlgorithmType name){
		algorithmName = name;
	}

	public AlgorithmType getAlgorithmName() {
		return algorithmName;
	}

	public float getCompresionGain() {
		return compresionGain;
	}

	public long getTotalTime() {
		return totalTime;
	}

	public long getTimePerChunck() {
		return timePerChunck;
	}
	
	
	public void setCompresionGain(float compresionGain) {
		this.compresionGain = compresionGain;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	public void updateTimePerChunck(long value){
		timePerChunck = ((timePerChunck * chunckTimes) + value) / (chunckTimes + 1);
		chunckTimes++;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((algorithmName == null) ? 0 : algorithmName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlgorithmData other = (AlgorithmData) obj;
		if (algorithmName != other.algorithmName)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AlgorithmData [algorithmName=" + algorithmName + ", compresionGain=" + compresionGain + ", totalTime=" + totalTime + ", timePerChunck="
				+ timePerChunck + "]";
	}
	
	
	
}
