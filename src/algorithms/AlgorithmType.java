package algorithms;

public enum AlgorithmType {
	GZIP("GZIP"),
	LZ4("LZ4"),
	LZ4HC("LZ4HC"),
	DEFLATE("DEFLATE"),
	SNAPPY("SNAPPY");
		
	private String name;
	
	AlgorithmType(String name){
		this.name = name;
	}
}
