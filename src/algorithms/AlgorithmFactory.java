package algorithms;

import net.jpountz.lz4.LZ4Factory;

public class AlgorithmFactory {

	private static Algorithm algorithm = null;
	private static LZ4Factory lz4Factory = null;

	/*
	 * Returns a LZ4Factory (static for performance reasons) 
	 */
	public static LZ4Factory getLZ4FactoryInstance(){

		if(lz4Factory == null){
			lz4Factory  = LZ4Factory.safeInstance();
		}
		
		return lz4Factory;
	}
	
	public static void killCurrentInstance(){
		if(algorithm != null){		
			algorithm.cancel(false);
		}
		algorithm = null;
	}
	
	public static Algorithm getNewInstane(AlgorithmType type){
		killCurrentInstance();

		switch (type) {
		case GZIP:
			algorithm = new GZIPAlgorithm();
			break;
		case DEFLATE:
			algorithm = new DeflateAlgorithm();
			break;
		case LZ4:
			algorithm = new LZ4Algorithm();
			break;
		case LZ4HC:
			algorithm = new LZ4_HCAlgorithm();
			break;
		case SNAPPY:
			algorithm = new SnappyAlgorithm();
		default:
			
			break;

		}

		return algorithm;
	}
	
	public static Algorithm getCurrentInstance(){
		return algorithm;
	}

}
