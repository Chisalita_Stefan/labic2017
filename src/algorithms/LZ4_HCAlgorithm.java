package algorithms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import net.jpountz.lz4.LZ4BlockInputStream;
import net.jpountz.lz4.LZ4BlockOutputStream;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import net.jpountz.lz4.LZ4SafeDecompressor;

public class LZ4_HCAlgorithm extends Algorithm {

	
	
	public LZ4_HCAlgorithm() {
		super();
		type = AlgorithmType.LZ4HC;
	}

	@Override
	protected void doCompression(FileInputStream inStream, FileOutputStream outStream) {

		int compressionLevel = 9;// TODO should be configurable
		LZ4Factory factory = AlgorithmFactory.getLZ4FactoryInstance();
		LZ4Compressor compressor = factory.highCompressor(compressionLevel);//TODO make static?
		LZ4BlockOutputStream blockOutStream = new LZ4BlockOutputStream(outStream, this.CHUNK_SIZE, compressor);
		try {
			copyBetweenStreams(inStream, blockOutStream);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				blockOutStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		/*
		///JUST FOR TESTS
		LZ4BlockInputStream blockIn = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(new File("C:/Users/Stefan/Desktop/t.pdf.gzip"));
			fos = new FileOutputStream(new File("C:/Users/Stefan/Desktop/t_uncompressed.pdf"));
			LZ4FastDecompressor decomp = factory.fastDecompressor();
			blockIn = new LZ4BlockInputStream(fis, decomp);
			copyBetweenStreams(blockIn, fos);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				blockIn.close();
				fis.close();
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		*/

	}

}
