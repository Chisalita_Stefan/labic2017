package algorithms;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

public class DeflateAlgorithm extends Algorithm {

	
	
	public DeflateAlgorithm() {
		super();
		type = AlgorithmType.DEFLATE;
	}

	@Override
	protected void doCompression(FileInputStream inStream,
			FileOutputStream outStream) {

		DeflaterOutputStream deflateStream = null;
		Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION); //de facut selectabil
	
		try {
			deflateStream = new DeflaterOutputStream(outStream, deflater);
			copyBetweenStreams(inStream, deflateStream);
		}catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				deflateStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}

}
