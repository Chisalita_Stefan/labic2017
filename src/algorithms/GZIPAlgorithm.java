package algorithms;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class GZIPAlgorithm extends Algorithm {

	
	
	public GZIPAlgorithm() {
		super();
		type = AlgorithmType.GZIP;
	}

	@Override
	protected void doCompression(FileInputStream inStream,
			FileOutputStream outStream) {

		GZIPOutputStream zip = null;
	
		try {
			zip = new GZIPOutputStream(outStream);
			copyBetweenStreams(inStream, zip);
		}catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				zip.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}

	
	
	
}
