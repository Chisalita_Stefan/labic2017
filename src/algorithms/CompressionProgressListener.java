package algorithms;

public interface CompressionProgressListener {

	public void compressionFinished(AlgorithmData data);
	
}
