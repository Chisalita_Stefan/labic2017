package algorithms;

import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public abstract class Algorithm extends SwingWorker<Void, Void> {

	
	private File fIn = null;
	private File fOut = null;
	private long originalFileSize = 0;
	private long chuncks = 0;
	private long chuncksDone = 0;
	private int progressDone = 0;
	protected final int CHUNK_SIZE = 4096;
	private JProgressBar progressBar = null;
	private long startTime;
	protected AlgorithmType type = null; // Only to be populated by the concrete type
	private AlgorithmData data = null;
	private CompressionProgressListener listener = null;
		
	public void registerProgressListener(CompressionProgressListener listener){
		this.listener = listener;
	}
	
	public void setProgressBar(JProgressBar progBar){
		this.progressBar = progBar;
	}
	
	abstract protected void doCompression(FileInputStream inStream,
			FileOutputStream outStream);

	public void compress(File fileIn, File fileOut) {

		startTime = System.nanoTime();
		data = new AlgorithmData(type);
		//create a copy, so that in case the file changes, it will not affect the thread!
		fIn = fileIn.getAbsoluteFile();
		fOut = fileOut.getAbsoluteFile();
		
		originalFileSize = fIn.length();	
		
		chuncks = (originalFileSize / CHUNK_SIZE) + 1;
		
		System.out.println("size = "+ fIn.length() + ", chunks = "+chuncks);
		
		execute();
		addPropertyChangeListener(evt->propertyChange(evt));

	}

	/**
     * Invoked when task's progress property changes.
     */
	private void propertyChange(PropertyChangeEvent evt) {
		 if ("progress" == evt.getPropertyName()) {
	            int progress = (Integer) evt.getNewValue();
	            if(progressBar != null){
	            	progressBar.setValue(progress);
	            }
	            
		        System.out.println("property change: "+progress);
	        } 
	}

	protected void copyBetweenStreams(InputStream in, OutputStream out)
			throws IOException {
		System.out.println("starts reading");
		byte buffer[] = new byte[CHUNK_SIZE];
		long startChunckReadTime = System.nanoTime();
		
		int readBytes = in.read(buffer);		
		while (readBytes > 0) {
			
			if(isCancelled()){
				System.out.println("canceled");
				return;
			}
			
			out.write(buffer, 0, readBytes);
			readBytes = in.read(buffer);
			chuncksDone++;
			progressDone = (int) (((float)chuncksDone/(float)chuncks) * 100.0);
			this.setProgress(progressDone);
		
			data.updateTimePerChunck(System.nanoTime()- startChunckReadTime);
			startChunckReadTime = System.nanoTime(); //restart timer
		}

		out.flush();
		System.out.println("flushed");
		
		
	}

	
	 @Override
     public Void doInBackground() {
        
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
		 
		 try {
				inStream = new FileInputStream(fIn);
				outStream = new FileOutputStream(fOut);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				if (inStream != null) { // maybe the input stream was opened before
										// exception
					try {
						inStream.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				return null;
			}

			doCompression(inStream, outStream);
			
			try {
				inStream.close();
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 
         return null;
     }

     /*
      * Executed in event dispatching thread
      */
     @Override
     public void done() {
         Toolkit.getDefaultToolkit().beep();
         System.out.println("Everything is done !!!!");
         
         if(isCancelled()){
        	 progressBar.setValue(0);
        	 return;
         }
         
         if(data == null){
        	 //should never hapen!
        	 return;
         }
         
         long timeElapsed = System.nanoTime() - startTime;
         data.setTotalTime(timeElapsed);
         float compresionGain = ((float)(originalFileSize - fOut.length())) / (float)originalFileSize ;
         data.setCompresionGain(compresionGain);
         System.out.println(data);
         
         if(listener != null){
        	 listener.compressionFinished(data);       	 
         }
     }
	
	

}
