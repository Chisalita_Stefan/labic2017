package algorithms;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.xerial.snappy.SnappyOutputStream;

public class SnappyAlgorithm extends Algorithm {

	public SnappyAlgorithm() {
		super();
		type = AlgorithmType.SNAPPY;
	}

	@Override
	protected void doCompression(FileInputStream inStream, FileOutputStream outStream) {

		try (SnappyOutputStream snappyStream = new SnappyOutputStream(outStream)) {
			copyBetweenStreams(inStream, snappyStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
